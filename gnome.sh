#!/bin/bash
##################################################################################################
##                                                                                              ##
##     ...............        .....         GNOME-Desktop-Installation                          ##
##    .doXMMMMxo;oMMMMO  .No KMMMMxdxOd.    Erst die Variablen anpassen!                        ##
##       kMMMM.   lMMMMO'N:  KMMMM.   dX    NICHT als root-user anmelden und ausführen!         ##
##       kMMMM.    ,WMMMW,   KMMMM.   cN                                                        ##
##       kMMMM.     .NMX.    KMMMM:;:x0.    Bei Fragen, komm einfach in den Matrix-Space:       ##
##       kMMMM.      00      KMMMMc:,.      https://matrix.to/#/#sontypiminternet:matrix.org    ##
##       :dddd      ;o       ldddd              So'n Typ Im Internet (2022)                     ##
##                                                                                              ##
##################################################################################################

##################
##  VARIABLE    ##
##################

country=Germany     # Hier dein Land für Reflector eintragen. Auf englisch: Germany, Switzerland, Austria,...

##########################
##  INSTALLATIONSSCRIPT ##
##########################

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c $country -a 12 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Sy

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload
# sudo virsh net-autostart default


sudo pacman -S gdm gnome gnome-extra firefox gnome-tweaks simplescreenrecorder arc-gtk-theme arc-icon-theme obs-studio vlc dina-font tamsyn-font ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts cantarell-fonts inter-font ttf-opensans gentium-plus-font ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts noto-fonts-cjk noto-fonts-emoji archlinux-wallpaper

sudo flatpak install -y spotify
sudo flatpak install -y kdenlive

sudo systemctl enable gdm
/bin/echo -e "\e[1;32mNEUSTART IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
